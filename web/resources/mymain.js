/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function infoInit(){
	$('.additionalInfo').parent().wrap("<tr class='additionalRow'></tr>");//okleja kazdy kontener z dodatkowymi danymi
	var newRow = $('tr.additionalRow');//przypisuje do zmiennej, w tym momencie zmienna newRow jest arrayem
	newRow.hide();//ukrywam
	$(newRow).each(function (index) {//dla kazdego nowego wiersza z danymi dokleja za jego rodzicem
		var newRowParent = $(this).parent();
		newRowParent.after(this);//this - pojedynczy wiersz z danymi, dokleja po rodzicu
	});
}

function infoToggle(elem){
	var elemTr = $(elem).parents('tr');//wyszukuje wsrod przodkow elementu tagu tr
	elemTr.next().toggle(300);
}
