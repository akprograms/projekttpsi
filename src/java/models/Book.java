/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package models;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Arek
 */

/*
Aplikacja księgarnia: możliwość dodania nowej książki, możliwość wyświetlenia szczegółów książki (tytuł, autor, cena, ilość posiadanych egzemplarzy), 
możliwość zmiany ilości sprzedanych/posiadanych ilości danej książki gdy klient kupi książkę, możliwość usunięcia już nie sprzedawanej książki.
*/

@ManagedBean(name = "bookModel")
@SessionScoped
public class Book implements Serializable
{
    private String title;
    private String author;
    private int price;
    private int publicationYear;
    private int count;
    private String ISBN;

    public Book() {}

    public Book(String title, String author, int price, int publicationYear, int count, String ISBN) {
        this.title = title;
        this.author = author;
        this.price = price;
        this.publicationYear = publicationYear;
        this.count = count;
        this.ISBN = ISBN;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getPublicationYear() {
        return publicationYear;
    }

    public void setPublicationYear(int publicationYear) {
        this.publicationYear = publicationYear;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }
    
    public void clearData()
    {
        title = null;
        author = null;
        price = 0;
        publicationYear = 0;
        count = 0;
        ISBN = null;
    }
    
    
    
}
