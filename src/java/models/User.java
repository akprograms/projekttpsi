/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package models;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Arek
 */

@ManagedBean(name = "userModel")
@SessionScoped
public class User implements Serializable
{
    private String login;
    private String password;

    public User() {}

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public boolean isEqual(User user)
    {
        if(this.login.equals(user.getLogin()) && this.password.equals(user.getPassword()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
}
