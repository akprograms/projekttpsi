/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Arek
 */

/*
Aplikacja księgarnia: możliwość dodania nowej książki, możliwość wyświetlenia szczegółów książki (tytuł, autor, cena, ilość posiadanych egzemplarzy), 
możliwość zmiany ilości sprzedanych/posiadanych ilości danej książki gdy klient kupi książkę, możliwość usunięcia już nie sprzedawanej książki.
*/

@ManagedBean(name = "bookstoreModel")
@SessionScoped
//@RequestScoped
public class Bookstore implements Serializable
{
    private List<Book> books;
    
    @PostConstruct
    public void init() 
    {
        books = new ArrayList<Book>();
        books.add(new Book("Lord of the Rings: Two towers", "J.R.R. Tolkien", 30, 1954, 5, "978-2-1234-5680-3"));
        books.add(new Book("Przenicowany Świat", "Arkadij i Borys Strugraccy", 28, 1969, 3, "978-3-6421-6483-2"));
        books.add(new Book("Najdłuższy dzień", "Cornelius Ryan", 25, 1959, 4, "978-2-5435-6456-3"));
        System.out.println("Ksiazki dodane!");
    }

    public Bookstore() {}
    
    public List<Book> getBooks()
    {
        return books;
    }
    
    public int getBookID(Book book)
    {
        return books.indexOf(book);
    }
    
    public void addBook(Book book)
    {
        books.add(book);
    }
    
    public String addBook(String title, String author, int price, int publicationYear, int count, String isbn)
    {
        books.add(new Book(title, author, price, publicationYear, count, isbn));
        return "index.html";
    }
    
    public void removeBook(Book book)
    {
        books.remove(book);
    }
    
    public void removeBookByID(int id)
    {
        books.remove(id);
    }
    
    public Book getBookByID(int id)
    {
        return books.get(id);
    }
    
    public void increaseBookCount(int bookId, int count)
    {
        books.get(bookId).setCount(books.get(bookId).getCount() + count);
    }
    
    public void decreaseBookCount(int bookId, int count)
    {
        books.get(bookId).setCount(books.get(bookId).getCount() - count);
    }
    
}
