/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Arek
 */

@ManagedBean(name = "systemUsersModel")
@SessionScoped
//@ViewScoped
public class SystemUsers implements Serializable
{
    private List<User> users;
    private boolean logged;
    
    @PostConstruct
    public void init() 
    {
        logged = false;
        users = new ArrayList<User>();
        users.add(new User("Admin", "Admin"));
        users.add(new User("Arek", "arek123"));
        users.add(new User("Prezes", "123123"));
        System.out.println("DODANI!");
    }

    public SystemUsers() {}

    public List<User> getUsers()
    {
        return users;
    }

    public void setUsers(List<User> users) 
    {
        this.users = users;
    }

    public boolean isLogged() 
    {
        return logged;
    }

    public void setLogged(boolean logged) 
    {
        this.logged = logged;
    }
    
    public void addUser(User user)
    {
        users.add(user);
    }
    
    private boolean isOnList(User user)
    {
        for (User user1 : users) 
        {
            System.out.println("porownanie " + user1.getLogin() + " " + user1.getPassword() + " Z " + user.getLogin() + " " + user.getPassword());
            if(user1.isEqual(user))
            {
                System.out.println("TACY SAMI! JEST NA LISCIE! ZWRACAM TRUE!");
                return true;
            }
        }
        return false;
    }
    
    public boolean isValidUser(User user)
    {
        return isOnList(user);
    }
    
    public boolean isValidUser(String login, String password)
    {
        System.out.println("login = " + login);
        System.out.println("password = " + password);
        User user = new User(login, password);
        System.out.println("users.contains(user) = " + isOnList(user));
        return isOnList(user);
    }
    
    public String login(User user)
    {
        if(isValidUser(user)) 
        {
            setLogged(true);
            System.out.println("isLogged() = " + isLogged());
            return "index.html";
        }
        else
        {
            return "loginError.html";
        }
    }
    
    public String login(String login, String password)
    {
        //User user = new User(login, password);
        if(isValidUser(login, password)) 
        {
            //setLogged(true);
            //System.out.println("isLogged() = " + isLogged());
            FacesContext context = FacesContext.getCurrentInstance();
            //HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
            context.getExternalContext().getSessionMap().put("username", login);
            context.getExternalContext().getSessionMap().put("isLogged", true);
            //session.setAttribute("username", login);
            //session.setAttribute("isLogged", true);
            return "index.html";
        }
        else
        {
            return "loginError.html";
        }
    }
    
    public void logout()
    {
        //HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        //session.invalidate();
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        if(isLogged())
        {
            //setLogged(false);
        }
    }
}
