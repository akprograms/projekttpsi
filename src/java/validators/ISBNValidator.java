/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author Arek
 */
public class ISBNValidator implements Validator
{
    private static final String ISBN_PATTERN = "[0-9]{3}-[0-9]-[0-9]{4}-[0-9]{4}-[0-9]";
    
    private Pattern pattern;
    private Matcher matcher;

    public ISBNValidator()
    {
	pattern = Pattern.compile(ISBN_PATTERN);
    }
    
    @Override
    public void validate(FacesContext fc, UIComponent uic, Object value) throws ValidatorException 
    {
        matcher = pattern.matcher(value.toString());
        if(!matcher.matches())
        {
            FacesMessage msg = new FacesMessage("ISBN validation failed.", "Niepoprawny format ISBN");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);
        }

    }
    
}
